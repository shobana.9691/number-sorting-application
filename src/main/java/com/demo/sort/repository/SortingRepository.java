package com.demo.sort.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.sort.entity.SortingEntity;

@Repository
public interface SortingRepository extends CrudRepository<SortingEntity, Double> {
 

}
