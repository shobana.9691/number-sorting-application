package com.demo.sort.controller;

import java.time.Duration;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.sort.dto.SortingDTO;
import com.demo.sort.exceptions.SortException;
import com.demo.sort.service.SortingService;



@RestController
public class SortingController {

	
	@Autowired
	private SortingService sortingService;
	
	@PostMapping("/sortRedirect/{inputText}")
	public SortingDTO sort(@PathVariable String inputText) throws SortException
	{
	
		Instant startTime = Instant.now();
		SortingDTO sortingDTO = new SortingDTO();
		sortingDTO.setInputNumbers(inputText);		
		sortingDTO = sortingService.sort(sortingDTO);
		sortingDTO.setTimeTaken(Duration.between(startTime, Instant.now()).toMillis());
		sortingService.persist(sortingDTO);
		return sortingDTO;
		

 	}
	

	
}
