package com.demo.sort.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SortingEntity {

	    @Id
	    private Double id;
	    
	
	
		public String inputNumbers;
		public String sortedNumbers;
		private long timeTaken;
		
	    public SortingEntity(Double id, String inputNumbers,
					String sortedNumbers, long timeTaken) {
				super();
				this.id = id;
				this.inputNumbers = inputNumbers;
				this.sortedNumbers = sortedNumbers;
				this.timeTaken = timeTaken;
			}
	    
		public SortingEntity() {
			// TODO Auto-generated constructor stub
		}

		public Double getId() {
			return id;
		}

		public void setId(Double id) {
			this.id = id;
		}
		public String getInputNumbers() {
			return inputNumbers;
		}
		public void setInputNumbers(String inputNumbers) {
			this.inputNumbers = inputNumbers;
		}
		public String getSortedNumbers() {
			return sortedNumbers;
		}
		public void setSortedNumbers(String sortedNumbers) {
			this.sortedNumbers = sortedNumbers;
		}
		public long getTimeTaken() {
			return timeTaken;
		}
		public void setTimeTaken(long timeTaken) {
			this.timeTaken = timeTaken;
		}
		
		

}
