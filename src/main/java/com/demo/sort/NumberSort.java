package com.demo.sort;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumberSort {

	public static void main(String[] args) {
		SpringApplication.run(NumberSort.class, args);
	}

}
