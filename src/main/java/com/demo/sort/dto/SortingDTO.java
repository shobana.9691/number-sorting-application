package com.demo.sort.dto;

public class SortingDTO {

	
	private int id;
	public String inputNumbers;
	public String sortedNumbers;
	private long timeTaken;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInputNumbers() {
		return inputNumbers;
	}
	public void setInputNumbers(String inputNumbers) {
		this.inputNumbers = inputNumbers;
	}
	public String getSortedNumbers() {
		return sortedNumbers;
	}
	public void setSortedNumbers(String sortedNumbers) {
		this.sortedNumbers = sortedNumbers;
	}
	public long getTimeTaken() {
		return timeTaken;
	}
	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	
	
}
