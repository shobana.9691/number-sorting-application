package com.demo.sort.service;


import com.demo.sort.dto.SortingDTO;
import com.demo.sort.exceptions.SortException;

public interface SortingService {

	SortingDTO sort(SortingDTO sortDTO) throws SortException;
	
	void persist(SortingDTO sortDTO);

	
	
}
