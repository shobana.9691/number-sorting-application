package com.demo.sort.service;

import com.demo.sort.dto.SortingDTO;
import com.demo.sort.entity.SortingEntity;
import com.demo.sort.exceptions.SortException;
import com.demo.sort.repository.SortingRepository;
import com.demo.sort.service.SortingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Arrays;


@Service
public class SortingServiceImpl implements SortingService{
	
	@Autowired
	public SortingRepository sortingRepository;

	public SortingDTO sort(SortingDTO sortDTO) throws SortException {
		
		try {
			String delimiter = ",";
			String [] inputNumbersInString = sortDTO.getInputNumbers().split(delimiter);
			int [] inputNumbers = new int[inputNumbersInString.length];
			for(int i=0; i<inputNumbersInString.length; i++) {
				inputNumbers[i] = Integer.parseInt(inputNumbersInString[i]);
				
				
			}
			Arrays.sort(inputNumbers);			 
			String sortedNumbers = Arrays.toString(inputNumbers);
			sortDTO.setSortedNumbers(sortedNumbers);
			
		} catch (NumberFormatException e)  {
			
		
		throw new SortException(e.getMessage());
			
		}
		return sortDTO;
	
	}

	public void persist(SortingDTO sortDTO) {
		SortingEntity sortEntity = new SortingEntity();
		sortEntity.setId(Math.random());
		sortEntity.setInputNumbers(sortDTO.getInputNumbers());
		sortEntity.setSortedNumbers(sortDTO.getSortedNumbers());
		sortEntity.setTimeTaken(sortDTO.getTimeTaken());
		sortingRepository.save(sortEntity);
		
	}


	
}
