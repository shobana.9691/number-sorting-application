# Sorting Application

## Usage

1. Clone the repository from Gitlab
2. Build using maven command : mvn clean install
3. Run the generated jar in command prompt using command : java -jar [jar-file-name].jar
4. Open browser and access URL - http://localhost:8080
